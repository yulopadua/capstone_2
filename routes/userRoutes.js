const express = require('express');
const router = express.Router();
const UserController = require('../controllers/userController');
const auth = require('../auth');

//User registration
router.post('/register', (req, res)=>{
	UserController.register(req.body).then(result => res.send(result))
});

//User authentication
router.post('/authenticate', (req, res)=>{
	UserController.authenticate(req.body).then(result => res.send(result))
});

router.get('/details', auth.verify, (req, res)=>{
	const user = auth.decode(req.headers.authorization)
	UserController.get({userId: user.id}).then(user => res.send(user))
})

//Set user as admin (Admin only)
router.put('/set-as-admin/:id', auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true){
		UserController.setAsAdmin(req.params.id).then(user => res.send(user))
	}else{
		res.status(403).send('Unauthorized')
	}
});

//Non-admin User checkout (Create Order)
router.post('/checkout', auth.verify, (req, res)=>{
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	if(auth.decode(req.headers.authorization).isAdmin === false){
		UserController.checkout(data).then(result => res.send(result));
	}else{
		res.status(403).send('Unauthorized')
	}
})

//Retrieve authenticated user's orders
router.get('/my-orders', auth.verify, (req, res)=>{
	const user = auth.decode(req.headers.authorization)
	UserController.myOrders({userId: user.id}).then(user => res.send(user))
})

//Retrieve all orders (Admin only)
router.get('/orders', auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true){
		UserController.getAll(req.body).then(user => res.send(user))
	}else{
		res.status(403).send('Unauthorized')
	}
})


module.exports = router