const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/productController');
const auth = require('../auth');

//Create Product (Admin only)
router.post('/create', auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true){
		ProductController.create(req.body).then(result => res.send(result))
	}else{
		res.status(403).send('Unauthorized')
	}
});

//Update Product information (Admin only)
router.put('/update/:id', auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true){
		ProductController.update(req.params.id, req.body).then(result => res.send(result))
	}else{
		res.status(403).send('Unauthorized')
	}
});

//Retrieve all active products
router.get('/active', (req, res)=>{
	ProductController.active(req.body).then(result => res.send(result))
});

//Retrieve single product
router.get('/:id', (req, res)=>{
	ProductController.get(req.params.id).then(result => res.send(result))
});

//Archive Product (Admin only)
router.put('/archive/:id', auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === true){
		ProductController.archive(req.params.id).then(result => res.send(result))
	}else{
		res.status(403).send('Unauthorized')
	}
});


module.exports = router