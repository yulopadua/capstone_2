const Product = require('../models/product');

module.exports.create = (params) => {
	let product = new Product({
		name: params.name,
		description: params.description,
		price: params.price,
	})

	return product.save().then((savedProduct, error)=>{
		return (error) ? false : true
	})
}

module.exports.update = (productId, newContent) =>{
	return Product.findByIdAndUpdate(productId, {

		name: newContent.name,
		description: newContent.description,
		price: newContent.price

		}).then((result, error) => {

		return (error) ? false : true
	})
}

module.exports.active = (params) =>{
	return Product.find({isActive: true}, {order: 0}).then(activeProducts => {
		return activeProducts
	})
}

module.exports.get = (params) =>{
	return Product.findById(params, {order: 0}).then(product => {
		return product
	})
}

module.exports.archive = (productId) =>{
	return Product.findByIdAndUpdate(productId, {isActive: false}).then((result, saveErr) => {

		return (saveErr) ? false : true
	})
}