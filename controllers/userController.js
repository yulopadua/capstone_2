const User = require('../models/user');
const Product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.register = (params) => {
	return User.find({email: params.email}).then(result => {
		if(result.length > 0){
			return false
		}else{
			let user = new User({
			email: params.email,
			password: bcrypt.hashSync(params.password, 10)
			})

			return user.save().then((user, error)=>{
				return (error) ? false : true
			})
		}
	})
}

module.exports.authenticate = (params) => {
	return User.findOne({email: params.email}).then(user => {
		if(user === null){
			return `${user}`
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if(isPasswordMatched){
			return {accessToken: auth.createAccessToken(user.toObject())}
		}else{
			return false
		}
	})
}

module.exports.get = (params) =>{
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.setAsAdmin = (userId) =>{
	return User.findByIdAndUpdate(userId, {isAdmin: true}).then((result, error) => {

		return (error) ? false : `${result.email} set as admin`
	})
}

module.exports.checkout = async (data) =>{
	var amount = null

	let x = await Product.findOne({_id: data.productId}).populate('_id').exec().then(products => {
		amount = products
	});

	var total = data.quantity * amount.price;

	let userOrderStatus = await User.findById(data.userId).then(user =>{

		user.order.push({productId: data.productId, totalAmount: total});
		return user.save().then((user, err)=>{
			if(err){
				return false;
			}else{
				return true
			}
		})
	})

	let productOrderStatus = await Product.findById(data.productId).then(product => {

		product.customer.push({userId: data.userId, quantity: data.quantity});
		return product.save().then((product, err) =>{
			if(err){
				return false;
			}else{
				return true
			}
		})
	})

	if(userOrderStatus && productOrderStatus){
		return true;
	}else{
		return false
	}
}

module.exports.myOrders = (params) =>{
	return User.findById(params.userId, {order: 1}).then(order => {
		return order
	})
}

module.exports.getAll = (params) =>{
	return User.find({}, {order: 1}).then(order => {
		return order
	})
}